import tensorflow as tf
from tensorflow.keras import backend as K


SMOOTH = 1e-5
REDUCE_AXES = [1]


def create_sparse_categorical_crossentropy():
    """
    Simply the spare categorical cross entropy.
    """
    return tf.keras.losses.SparseCategoricalCrossentropy()


def create_categorical_crossentropy():
    """
    Simply return the categorical crossentropy.

    Use this if the ground-truth targets are already one-hot encoded.
    """
    return tf.keras.losses.CategoricalCrossentropy()


def create_categorical_crossentropy_ignoring_void(void_class: int) -> callable:
    """Create categorical crossentropy loss ignoring a void class.
    
    Parameters
    ----------
    void_class : int
        Label of void class that is ignored.

    Returns
    -------
    ce_ignoring_void : callable
        The loss function callable accepting parameters y_true and y_pred.
    """
    def ce_ignoring_void(y_true, y_pred):
        if y_true.shape[-1] > y_pred.shape[-1]:
            n_classes_incl_void = y_true.shape[-1]
            gather_axes = [i for i in range(n_classes_incl_void) if i != void_class]
            y_true = tf.gather(y_true, gather_axes, axis=-1)
        else:
            n_classes = y_true.shape[-1]
            gather_idcs = list(range(n_classes-1))
            y_true = tf.gather(y_true, gather_idcs, axis=-1)
            y_pred = tf.gather(y_pred, gather_idcs, axis=-1)

        return tf.keras.losses.categorical_crossentropy(y_true, y_pred)

    return ce_ignoring_void


def create_dice_loss() -> callable:
    """Create plain dice loss.
    
    Return
    ------
    dice_loss : callable
        Dice loss accepting parameters y_true and y_pred.
    """
    def dice_loss(y_true, y_pred):
        """
        Segmentation map and prediction come in as one-hot encoded vectors.
        """
        y_true = tf.cast(y_true, y_pred.dtype)
        tp = tf.reduce_sum(tf.multiply(y_true, y_pred), axis=REDUCE_AXES)
        fp = tf.reduce_sum(y_pred, axis=REDUCE_AXES) - tp
        fn = tf.reduce_sum(y_true, axis=REDUCE_AXES) - tp
        score = ((2*tp + SMOOTH) / (2*tp + SMOOTH + fp + fn))

        return 1 - tf.keras.backend.mean(score)    

    return dice_loss


# Custom dice loss that _really_ ignores void areas
def create_dice_loss_ignoring_void(void_class) -> callable:
    """Create custom dice loss ignoring the void class.

    Parameters
    ----------
    void_class : int
        Label of void class that is ignored.
    
    Return
    ------
    dice_loss_ignoring_void : callable
        Dice loss accepting parameters y_true and y_pred.
    """
    def dice_loss_ignoring_void(y_true, y_pred):
        """
        Segmentation map and prediction come in as one-hot encoded vectors. `y_true` can have
        one more channel than `y_pred` due to the void label.
        """
        y_true = void_adjust_true(y_true, y_pred, void_class)

        tp = tf.reduce_sum(tf.multiply(y_true, y_pred), axis=REDUCE_AXES)
        fp = tf.reduce_sum(y_pred, axis=REDUCE_AXES) - tp
        fn = tf.reduce_sum(y_true, axis=REDUCE_AXES) - tp
        score = (2*tp + SMOOTH) / (2*tp + SMOOTH + fp + fn)
        
        return 1 - tf.keras.backend.mean(score)

    return dice_loss_ignoring_void


def void_adjust_true(y_true, y_pred, void_class):
    """Adjust the ground truth vector to match the prediction everywhere marked void.
    
    Parameters
    ----------
    y_true : tf.Tensor
        Ground truth segmentation map (including void label).
    y_pred : tf.Tensor
        Predicted segmentation map. May include void class or not.
    void_class : int
        Label of void class.

    Return
    ------
    y_true : tf.Tensor
        Modified ground truth segmentation map matching `y_pred` everywhere there was void.
    """
    if y_true.shape[-1] > y_pred.shape[-1]:
        n_classes_incl_void = y_true.shape[-1]
        gather_axes = [i for i in range(n_classes_incl_void) if i != void_class]
        y_true = tf.where(y_true[..., void_class, tf.newaxis] > 0, y_pred, tf.gather(y_true, gather_axes, axis=-1))
    else:
        y_true = tf.where(y_true[..., void_class, tf.newaxis] > 0, y_pred, y_true)

    return y_true


# Combination of dice loss (global) and categorical crossentropy (local)
def create_dl_ce_ignoring_void(void_class) -> callable:
    """Create loss combining custom dice loss and categorical crossentropy.

    Parameters
    ----------
    void_class : int
        Label of void class that is ignored.

    Return
    ------
    dl_ce_ignoring_void : callable
        Loss function accepting parameters y_true and y_pred.
    """
    dice_loss = create_dice_loss_ignoring_void(void_class)
    categorical_crossentropy = create_categorical_crossentropy_ignoring_void(void_class)

    def dl_ce_ignoring_void(y_true, y_pred):
        dl = dice_loss(y_true, y_pred)
        ce = categorical_crossentropy(y_true, y_pred)

        loss = dl + ce

        return tf.reduce_mean(loss)

    return dl_ce_ignoring_void


def create_pixel_accuracy(n_classes):
    """
    Fraction of pixels that have been labelled correctly. Prediction is based 
    on argmax-ing the logits.
    """
    def pixel_accuracy(y_true, y_pred):
        y_pred = K.reshape(y_pred, (-1, n_classes))
        y_true = K.argmax(y_true, axis=-1)  # Removing one-hot encoding by data-generator
        y_true = tf.cast(K.flatten(y_true), dtype=tf.int64)
        return K.sum(tf.cast(K.equal(y_true, K.argmax(y_pred, axis=-1)), dtype=tf.float32)) / tf.cast(tf.size(y_true), dtype=tf.float32)
        
    return pixel_accuracy


def create_pixel_accuracy_ignoring_void(n_classes):
    """
    Fraction of pixels that have been labelled correctly. Prediction is based 
    on argmax-ing the logits. Here the void pixels are neglected, with the first
    class being the assumed to be the void class.
    """
    void_class = 0
    
    def pixel_accuracy_ignoring_void(y_true, y_pred):
        y_pred = K.reshape(y_pred, (-1, n_classes))
        y_true = K.argmax(y_true, axis=-1)  # Removing one-hot encoding by data-generator
        y_true = tf.cast(K.flatten(y_true), dtype=tf.int64)
        legal_labels = ~K.equal(y_true, void_class)
        return K.sum(tf.cast(legal_labels & K.equal(y_true, K.argmax(y_pred, axis=-1)), dtype=tf.float32)) / K.sum(tf.cast(legal_labels, dtype=tf.float32))
    
    return pixel_accuracy_ignoring_void


def create_iou_ignoring_void(void_class, single_label=None):
    """Create IoU metric ignoring the void class.
    
    Parameters
    ----------
    void_class : int
        Label of the void class to ignore.
    single_label : int, optional
        Single label for which IoU should be calculated. Defaults to None (use all labels).
    
    Return
    ------
    iou_ignoring_void : callable
        Function calculating the IoU metric between parameters y_true and y_pred.
    """
    def iou_ignoring_void(y_true, y_pred):
        """
        Segmentation map and prediction come in as one-hot encoded vectors. `y_true` can have 
        one more channel than `y_pred` due to the void label.
        """
        y_true = void_adjust_true(y_true, y_pred, void_class)

        if single_label is not None:
            y_true = tf.gather(y_true, [single_label], axis=-1)
            y_pred = tf.gather(y_pred, [single_label], axis=-1)
        
        intersection = tf.reduce_sum(tf.multiply(y_true, y_pred), axis=REDUCE_AXES)
        union = tf.reduce_sum(y_true + y_pred, axis=REDUCE_AXES) - intersection
        score = (intersection + SMOOTH) / (union + SMOOTH)

        return tf.keras.backend.mean(score)

    if single_label is not None:
        iou_ignoring_void.__name__ = f"iou_ignoring_void_label_{single_label}"
        
    return iou_ignoring_void


def create_iou(single_label=None):
    """Create plain IoU score.
    
    Parameters
    ----------
    single_label : int
        Single label for which IoU should be calculated. Defaults to None (use all labels).

    Return
    ------
    iou : callable
        Dice loss accepting parameters y_true and y_pred.
    """
    def iou(y_true, y_pred):
        """
        Segmentation map and prediction come in as one-hot encoded vectors."""
        if single_label is not None:
            y_true = tf.gather(y_true, [single_label], axis=-1)
            y_pred = tf.gather(y_pred, [single_label], axis=-1)

        intersection = tf.reduce_sum(tf.multiply(y_true, y_pred), axis=REDUCE_AXES)
        union = tf.reduce_sum(y_true + y_pred, axis=REDUCE_AXES) - intersection
        score = (intersection + SMOOTH) / (union + SMOOTH)

        return tf.keras.backend.mean(score)

    if single_label is not None:
        iou.__name__ = f"iou_label_{single_label}"

    return iou
