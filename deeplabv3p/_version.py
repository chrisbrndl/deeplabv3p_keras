# __version__ = "1.0.0" #6.8.2020
# __version__ = "1.2.0"  # 12.02.2021 - Two-headed, custom dice loss, LL-feature upsampling for mobilenet backbone
# __version__ = "1.2.1"  # 12.02.2021 - Adding dice loss for non-void data
__version__ = "1.2.2"  # 12.02.2021 - Adding iou score for non-void data
