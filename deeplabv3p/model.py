################################################################################################################
#
# Major parts of the following code were talen from:
# https://github.com/bonlime/keras-deeplab-v3-plus
#
################################################################################################################


import os
import numpy as np
import tensorflow as tf

from tensorflow.keras.models import Model
from tensorflow.keras.activations import relu
from tensorflow.keras.layers import Layer, Conv2D, DepthwiseConv2D, ZeroPadding2D, Lambda, AveragePooling2D, Input, Activation, Concatenate, Add, Reshape, BatchNormalization, Dropout 
from tensorflow.keras import backend as K
from tensorflow.keras import layers

if tf.__version__[0] == "2":
    resize_bilinear = tf.image.resize
else:
    from tensorflow.image import resize_bilinear
        

################################################################################################################
## pretrained model paths ######################################################################################
################################################################################################################   

file_path = os.path.dirname(os.path.abspath(__file__))

PATH_WEIGHTS_MOBILENETV2_PASCAL_VOC = '{}/pretrained_models/mobilenetv2_pascal_voc.h5'.format(file_path)
PATH_WEIGHTS_MOBILENETV2_CITYSCAPES = '{}/pretrained_models/mobilenetv2_cityscapes.h5'.format(file_path)

PATH_WEIGHTS_XCEPTION_PASCAL_VOC = '{}/pretrained_models/xception_pascal_voc.h5'.format(file_path)
PATH_WEIGHTS_XCEPTION_CITYSCAPES = '{}/pretrained_models/xception_cityscapes.h5'.format(file_path)


################################################################################################################
## some helper #################################################################################################
################################################################################################################   
    
    
def SepConv_BN(x, filters, prefix, stride=1, kernel_size=3, rate=1, depth_activation=False, epsilon=1e-3):
    """ SepConv with BN between depthwise & pointwise. Optionally add activation after BN
        Implements right "same" padding for even kernel sizes
        Args:
            x: input tensor
            filters: num of filters in pointwise convolution
            prefix: prefix before name
            stride: stride at depthwise conv
            kernel_size: kernel size for depthwise convolution
            rate: atrous rate for depthwise convolution
            depth_activation: flag to use activation between depthwise & pointwise convs
            epsilon: epsilon to use in BN layer
    """

    if stride == 1:
        depth_padding = 'same'
    else:
        kernel_size_effective = kernel_size + (kernel_size - 1) * (rate - 1)
        pad_total = kernel_size_effective - 1
        pad_beg = pad_total // 2
        pad_end = pad_total - pad_beg
        x = ZeroPadding2D((pad_beg, pad_end))(x)
        depth_padding = 'valid'

    if not depth_activation:
        x = Activation('relu')(x)
    x = DepthwiseConv2D((kernel_size, kernel_size), strides=(stride, stride), dilation_rate=(rate, rate),
                        padding=depth_padding, use_bias=False, name=prefix + '_depthwise')(x)
    x = BatchNormalization(name=prefix + '_depthwise_BN', epsilon=epsilon)(x)
    if depth_activation:
        x = Activation('relu')(x)
    x = Conv2D(filters, (1, 1), padding='same',
               use_bias=False, name=prefix + '_pointwise')(x)
    x = BatchNormalization(name=prefix + '_pointwise_BN', epsilon=epsilon)(x)
    if depth_activation:
        x = Activation('relu')(x)

    return x


def _conv2d_same(x, filters, prefix, stride=1, kernel_size=3, rate=1):
    """Implements right 'same' padding for even kernel sizes
        Without this there is a 1 pixel drift when stride = 2
        Args:
            x: input tensor
            filters: num of filters in pointwise convolution
            prefix: prefix before name
            stride: stride at depthwise conv
            kernel_size: kernel size for depthwise convolution
            rate: atrous rate for depthwise convolution
    """
    if stride == 1:
        return Conv2D(filters,
                      (kernel_size, kernel_size),
                      strides=(stride, stride),
                      padding='same', use_bias=False,
                      dilation_rate=(rate, rate),
                      name=prefix)(x)
    else:
        kernel_size_effective = kernel_size + (kernel_size - 1) * (rate - 1)
        pad_total = kernel_size_effective - 1
        pad_beg = pad_total // 2
        pad_end = pad_total - pad_beg
        x = ZeroPadding2D((pad_beg, pad_end))(x)
        return Conv2D(filters,
                      (kernel_size, kernel_size),
                      strides=(stride, stride),
                      padding='valid', use_bias=False,
                      dilation_rate=(rate, rate),
                      name=prefix)(x)


def _xception_block(inputs, depth_list, prefix, skip_connection_type, stride,
                    rate=1, depth_activation=False, return_skip=False):
    """ Basic building block of modified Xception network
        Args:
            inputs: input tensor
            depth_list: number of filters in each SepConv layer. len(depth_list) == 3
            prefix: prefix before name
            skip_connection_type: one of {'conv','sum','none'}
            stride: stride at last depthwise conv
            rate: atrous rate for depthwise convolution
            depth_activation: flag to use activation between depthwise & pointwise convs
            return_skip: flag to return additional tensor after 2 SepConvs for decoder
            """
    residual = inputs
    for i in range(3):
        residual = SepConv_BN(residual,
                              depth_list[i],
                              prefix + '_separable_conv{}'.format(i + 1),
                              stride=stride if i == 2 else 1,
                              rate=rate,
                              depth_activation=depth_activation)
        if i == 1:
            skip = residual
    if skip_connection_type == 'conv':
        shortcut = _conv2d_same(inputs, depth_list[-1], prefix + '_shortcut',
                                kernel_size=1,
                                stride=stride)
        shortcut = BatchNormalization(name=prefix + '_shortcut_BN')(shortcut)
        outputs = layers.add([residual, shortcut])
    elif skip_connection_type == 'sum':
        outputs = layers.add([residual, inputs])
    elif skip_connection_type == 'none':
        outputs = residual
    if return_skip:
        return outputs, skip
    else:
        return outputs

    
def _make_divisible(v, divisor, min_value=None):
    if min_value is None:
        min_value = divisor
    new_v = max(min_value, int(v + divisor / 2) // divisor * divisor)
    # Make sure that round down does not go down by more than 10%.
    if new_v < 0.9 * v:
        new_v += divisor
    return new_v


def _inverted_res_block(inputs, expansion, stride, alpha, filters, block_id, skip_connection, rate=1,
                        bn_momentum=0.999):
    """
    Pass through inverted res-block used in mobilnets.
    """
    in_channels = inputs.shape[-1]
    
    pointwise_conv_filters = int(filters * alpha)
    pointwise_filters = _make_divisible(pointwise_conv_filters, 8)
    x = inputs
    prefix = 'expanded_conv_{}_'.format(block_id)
    
    if block_id:
        x = Conv2D(expansion * in_channels, kernel_size=1, padding='same', use_bias=False, activation=None, name=prefix + 'expand')(x)
        x = BatchNormalization(epsilon=1e-3, momentum=bn_momentum, name=prefix + 'expand_BN')(x)
        x = Lambda(lambda x: relu(x, max_value=6.), name=prefix + 'expand_relu')(x)
    else:
        prefix = 'expanded_conv_'

    x = DepthwiseConv2D(kernel_size=3, strides=stride, activation=None, use_bias=False, padding='same', dilation_rate=(rate, rate), name=prefix + 'depthwise')(x)
    x = BatchNormalization(epsilon=1e-3, momentum=bn_momentum, name=prefix + 'depthwise_BN')(x)
    x = Lambda(lambda x: relu(x, max_value=6.), name=prefix + 'depthwise_relu')(x)
    x = Conv2D(pointwise_filters, kernel_size=1, padding='same', use_bias=False, activation=None, name=prefix + 'project')(x)
    x = BatchNormalization(epsilon=1e-3, momentum=bn_momentum, name=prefix + 'project_BN')(x)

    if skip_connection:
        return Add(name=prefix + 'add')([inputs, x])

    return x


def _aspp(inputs, input_shape, OS, atrous_rates, prefix, backbone):
    # Atrous Spatial Pyramid Pooling
    b4 = AveragePooling2D(pool_size=(int(np.ceil(input_shape[0] / OS)), int(np.ceil(input_shape[1] / OS))))(inputs)   
    b4 = Conv2D(256, (1, 1), padding='same', use_bias=False, name=prefix+'image_pooling')(b4)
    b4 = BatchNormalization(name=prefix+'image_pooling_BN', epsilon=1e-5)(b4)
    b4 = Activation('relu')(b4)
    b4 = Lambda(lambda x: resize_bilinear(x,size=(int(np.ceil(input_shape[0]/OS)), int(np.ceil(input_shape[1]/OS)))))(b4)

    b0 = Conv2D(256, (1, 1), padding='same', use_bias=False, name=prefix+'aspp0')(inputs)     # simple 1x1
    b0 = BatchNormalization(name=prefix+'aspp0_BN', epsilon=1e-5)(b0)
    b0 = Activation('relu', name=prefix+'aspp0_activation')(b0)

    if backbone == 'xception':
        b1 = SepConv_BN(inputs, 256, prefix+'aspp1', rate=atrous_rates[0], depth_activation=True, epsilon=1e-5) # rate = 6 (12)
        b2 = SepConv_BN(inputs, 256, prefix+'aspp2', rate=atrous_rates[1], depth_activation=True, epsilon=1e-5) # rate = 12 (24)
        b3 = SepConv_BN(inputs, 256, prefix+'aspp3', rate=atrous_rates[2], depth_activation=True, epsilon=1e-5) # rate = 18 (36)
        x = Concatenate()([b4, b0, b1, b2, b3])
    else:
        x = Concatenate()([b4, b0])

    x = Conv2D(256, (1, 1), padding='same', use_bias=False, name=prefix+'concat_projection')(x)
    x = BatchNormalization(name=prefix+'concat_projection_BN', epsilon=1e-5)(x)
    x = Activation('relu')(x)
    x = Dropout(0.1)(x)

    return x


def _decoder(x, input_shape, skip1, classes, weights, activation, prefix, backbone, mobile_attach_skip=False):
    # Decoder
    if backbone == 'xception':
        x = Lambda(lambda x: resize_bilinear(x,size=(int(np.ceil(input_shape[0]/4)), int(np.ceil(input_shape[1]/4)))))(x)
        
        dec_skip1 = Conv2D(48, (1, 1), padding='same', use_bias=False, name=prefix+'feature_projection0')(skip1)
        dec_skip1 = BatchNormalization(name=prefix+'feature_projection0_BN', epsilon=1e-5)(dec_skip1)
        dec_skip1 = Activation('relu')(dec_skip1)

        x = Concatenate()([x, dec_skip1])
        
        x = SepConv_BN(x, 256, prefix+'decoder_conv0', depth_activation=True, epsilon=1e-5)
        x = SepConv_BN(x, 256, prefix+'decoder_conv1', depth_activation=True, epsilon=1e-5)

    if backbone == 'mobilenetv2' and mobile_attach_skip:
        x = Lambda(lambda x: resize_bilinear(x,
                                             size=(int(np.ceil(input_shape[0]/4)), int(np.ceil(input_shape[1]/4)))
                                             )
                   )(x)

        dec_skip1 = Conv2D(48, (1, 1), padding='same', use_bias=False, name=prefix+'feature_projection0')(skip1)
        dec_skip1 = BatchNormalization(name=prefix+'feature_projection0_BN', epsilon=1e-5)(dec_skip1)
        dec_skip1 = Activation('relu')(dec_skip1)

        x = Concatenate()([x, dec_skip1])

        x = Conv2D(256, (1, 1), padding='same', use_bias=True, name=prefix+'upsample_conv0')(x)

    if (weights == 'pascal_voc' and classes == 21) or (weights == 'cityscapes' and classes == 19):
        last_layer_name = 'logits_semantic'
    else:
        last_layer_name = 'custom_logits_semantic'
        
    x = Conv2D(classes, (1, 1), padding='same', name=prefix+last_layer_name)(x)
    x = Lambda(lambda x: resize_bilinear(x,size=(input_shape[0],input_shape[1])))(x)
    x = Reshape((input_shape[0]*input_shape[1], classes))(x)

    if classes == 1:
        # Force sigmoid activation for one-class problems. Ensures safety when keeping softmax but reducing number of classes.
        activation = 'sigmoid'
    x = Activation(activation, name=prefix+'_output')(x)

    return x



################################################################################################################
## DeepLabV3p ##################################################################################################
################################################################################################################   


def DeepLabV3p(weights=None, 
               input_shape=(512, 512, 3), 
               classes=21,
               activation='softmax',
               backbone='mobilenetv2', 
               OS=16, 
               alpha=1.0,
               two_headed=False,
               classes_head2=None,
               head_names=['head1', 'head2'],
               bn_momentum=0.999,
               mobile_head1_skip=False,
               mobile_head2_skip=False,
               ):
    """ Instantiates the Deeplabv3+ architecture
    Optionally loads weights pre-trained
    on PASCAL VOC. This model is available for TensorFlow only,
    and can only be used with inputs following the TensorFlow
    data format `(width, height, channels)`.
    # Arguments
        weights: one of 'pascal_voc' (pre-trained on pascal voc)
            or None (random initialization)
        input_shape: shape of input image. format HxWxC
            PASCAL VOC model was trained on (512,512,3) images
        classes: number of desired classes. If classes != 21,
            last layer is initialized randomly.
        activation: Activation function to use in final prediction layer.
        backbone: backbone to use. one of {'xception','mobilenetv2'}
        OS: determines input_shape/feature_extractor_output ratio. One of {8,16}.
            Used only for xception backbone.
        alpha: controls the width of the MobileNetV2 network. This is known as the
            width multiplier in the MobileNetV2 paper.
                - If `alpha` < 1.0, proportionally decreases the number
                    of filters in each layer.
                - If `alpha` > 1.0, proportionally increases the number
                    of filters in each layer.
                - If `alpha` = 1, default number of filters from the paper
                    are used at each layer.
            Used only for mobilenetv2 backbone
        two_headed: Attach a second ASPP and decoder to the network to allow multi-output
            training.
        classes_head2: Number of classes for the second output head if `two_headed` is True.
        head_names: List of two names for the different output heads if `two_headed` is True.
        bn_momentum: Momentum of BatchNormalization layers. Only applied to batch normalization layers in inverted
            residual blocks.
    # Returns
        A Keras model instance.
    # Raises
        RuntimeError: If attempting to run this model with a
            backend that does not support separable convolutions.
        ValueError: in case of invalid argument for `weights` or `backbone`
    """
    
    # check the inputs 
    if not (weights in {'pascal_voc', None}):
        raise ValueError('The `weights` argument should be either `None` (random initialization) or `pascal_voc` (pre-trained on PASCAL VOC)')

    if not (backbone in {'xception', 'mobilenetv2'}):
        raise ValueError('The `backbone` argument should be either `xception`  or `mobilenetv2` ')

    if two_headed and classes_head2 is None:
        raise ValueError('The `classes_head2` argument must be set for a two-headed network')
    if two_headed and len(head_names) != 2:
        raise ValueError('The `head_names` argument must specify two names for the output heads')

        
    # check the backend
    if K.backend() != 'tensorflow':
        raise RuntimeError('The Deeplabv3+ model is only available with the TensorFlow backend.')
    
    # define and normlize the input tensor
    img_input = Input(shape=input_shape)
    # Fix shapes when using pre-trained weights with grayscale images
    if weights == 'pascal_voc' and input_shape[-1] == 1:
        img_input2 = Concatenate()([img_input, img_input, img_input])
    else:
        img_input2 = img_input

    batches_input = Lambda(lambda x: x/127.5 - 1)(img_input2)

    
    # Feature extraction: Xception backbone
    if backbone == 'xception':
        if OS == 8:
            entry_block3_stride = 1
            middle_block_rate = 2  # ! Not mentioned in paper, but required
            exit_block_rates = (2, 4)
            atrous_rates = (12, 24, 36)
        else:
            entry_block3_stride = 2
            middle_block_rate = 1
            exit_block_rates = (1, 2)
            atrous_rates = (6, 12, 18)
        
        x = Conv2D(32, (3, 3), strides=(2, 2), name='entry_flow_conv1_1', use_bias=False, padding='same')(batches_input)
            
        x = BatchNormalization(name='entry_flow_conv1_1_BN')(x)
        x = Activation('relu')(x)

        x = _conv2d_same(x, 64, 'entry_flow_conv1_2', kernel_size=3, stride=1)
        x = BatchNormalization(name='entry_flow_conv1_2_BN')(x)
        x = Activation('relu')(x)

        x = _xception_block(x, [128, 128, 128], 'entry_flow_block1', skip_connection_type='conv', stride=2, depth_activation=False)
        
        x, skip1 = _xception_block(x, [256, 256, 256], 'entry_flow_block2', skip_connection_type='conv', stride=2, depth_activation=False, return_skip=True)

        x = _xception_block(x, [728, 728, 728], 'entry_flow_block3', skip_connection_type='conv', stride=entry_block3_stride, depth_activation=False)
        for i in range(16):
            x = _xception_block(x, [728, 728, 728], 'middle_flow_unit_{}'.format(i + 1), skip_connection_type='sum', stride=1, rate=middle_block_rate, depth_activation=False)

        x = _xception_block(x, [728, 1024, 1024], 'exit_flow_block1', skip_connection_type='conv', stride=1, rate=exit_block_rates[0],depth_activation=False)
        x = _xception_block(x, [1536, 1536, 2048], 'exit_flow_block2', skip_connection_type='none', stride=1, rate=exit_block_rates[1], depth_activation=True)

    # Feature extraction: MobileNet backbone
    else:
        # Aspp and the low-level feature connection is not used for the MobileNet backbone
        atrous_rates = None
        skip1 = None
        OS = 8
        
        first_block_filters = _make_divisible(32 * alpha, 8)
        
        x = Conv2D(first_block_filters, kernel_size=3, strides=(2, 2), padding='same', use_bias=False, name='Conv')(batches_input)
        
        x = BatchNormalization(epsilon=1e-3, momentum=bn_momentum, name='Conv_BN')(x)
        
        x = Lambda(lambda x: relu(x, max_value=6.))(x)

        x = _inverted_res_block(x, filters=16, alpha=alpha, stride=1, rate=1, expansion=1, block_id=0,
                                skip_connection=False, bn_momentum=bn_momentum)

        x = _inverted_res_block(x, filters=24, alpha=alpha, stride=2, rate=1, expansion=6, block_id=1,
                                skip_connection=False, bn_momentum=bn_momentum)
        x = _inverted_res_block(x, filters=24, alpha=alpha, stride=1, rate=1, expansion=6, block_id=2,
                                skip_connection=True, bn_momentum=bn_momentum)
        if mobile_head1_skip or mobile_head2_skip:
            skip1 = x

        x = _inverted_res_block(x, filters=32, alpha=alpha, stride=2, rate=1, expansion=6, block_id=3,
                                skip_connection=False, bn_momentum=bn_momentum)
        x = _inverted_res_block(x, filters=32, alpha=alpha, stride=1, rate=1, expansion=6, block_id=4,
                                skip_connection=True, bn_momentum=bn_momentum)
        x = _inverted_res_block(x, filters=32, alpha=alpha, stride=1, rate=1, expansion=6, block_id=5,
                                skip_connection=True, bn_momentum=bn_momentum)

        x = _inverted_res_block(x, filters=64, alpha=alpha, stride=1, rate=1, expansion=6, block_id=6,
                                skip_connection=False, bn_momentum=bn_momentum)
        x = _inverted_res_block(x, filters=64, alpha=alpha, stride=1, rate=2, expansion=6, block_id=7,
                                skip_connection=True, bn_momentum=bn_momentum)
        x = _inverted_res_block(x, filters=64, alpha=alpha, stride=1, rate=2, expansion=6, block_id=8,
                                skip_connection=True, bn_momentum=bn_momentum)
        x = _inverted_res_block(x, filters=64, alpha=alpha, stride=1, rate=2, expansion=6, block_id=9,
                                skip_connection=True, bn_momentum=bn_momentum)

        x = _inverted_res_block(x, filters=96, alpha=alpha, stride=1, rate=2, expansion=6, block_id=10,
                                skip_connection=False, bn_momentum=bn_momentum)
        x = _inverted_res_block(x, filters=96, alpha=alpha, stride=1, rate=2, expansion=6, block_id=11,
                                skip_connection=True, bn_momentum=bn_momentum)
        x = _inverted_res_block(x, filters=96, alpha=alpha, stride=1, rate=2, expansion=6, block_id=12,
                                skip_connection=True, bn_momentum=bn_momentum)

        x = _inverted_res_block(x, filters=160, alpha=alpha, stride=1, rate=2, expansion=6, block_id=13,
                                skip_connection=False, bn_momentum=bn_momentum)
        x = _inverted_res_block(x, filters=160, alpha=alpha, stride=1, rate=4, expansion=6, block_id=14,
                                skip_connection=True, bn_momentum=bn_momentum)
        x = _inverted_res_block(x, filters=160, alpha=alpha, stride=1, rate=4, expansion=6, block_id=15,
                                skip_connection=True, bn_momentum=bn_momentum)

        x = _inverted_res_block(x, filters=320, alpha=alpha, stride=1, rate=4, expansion=6, block_id=16,
                                skip_connection=False, bn_momentum=bn_momentum)
    
    # Add atrous spatial pyramid pooling layers
    head1_prefix = head_names[0] if two_headed else 'deeplab'
    aspp = _aspp(x, input_shape, OS, atrous_rates, head1_prefix, backbone)
    if two_headed:
        aspp2 = _aspp(x, input_shape, OS, atrous_rates, head_names[1], backbone)

    # Decoder
    decoder = _decoder(aspp, input_shape, skip1, classes, weights, activation, head1_prefix, backbone, mobile_head1_skip)
    if two_headed:
        decoder2 = _decoder(aspp2, input_shape, skip1, classes_head2, weights, activation, head_names[1], backbone, mobile_head2_skip)
    
    # Complete model and load weights
    outputs = decoder if not two_headed else [decoder, decoder2]
    model = Model(img_input, outputs=outputs, name='deeplabv3p')
    
    if weights=='pascal_voc':
        if backbone=='mobilenetv2':
            model.load_weights(PATH_WEIGHTS_MOBILENETV2_PASCAL_VOC, by_name=True)
        if backbone=='xception':
            model.load_weights(PATH_WEIGHTS_XCEPTION_PASCAL_VOC, by_name=True)
    
    return model






# """ Deeplabv3+ model for Keras.
# This model is based on TF repo:
# https://github.com/tensorflow/models/tree/master/research/deeplab
# On Pascal VOC, original model gets to 84.56% mIOU
# Now this model is only available for the TensorFlow backend,
# due to its reliance on `SeparableConvolution` layers, but Theano will add
# this layer soon.
# MobileNetv2 backbone is based on this repo:
# https://github.com/JonathanCMitchell/mobilenet_v2_keras
# # Reference
# - [Encoder-Decoder with Atrous Separable Convolution
#     for Semantic Image Segmentation](https://arxiv.org/pdf/1802.02611.pdf)
# - [Xception: Deep Learning with Depthwise Separable Convolutions]
#     (https://arxiv.org/abs/1610.02357)
# - [Inverted Residuals and Linear Bottlenecks: Mobile Networks for
#     Classification, Detection and Segmentation](https://arxiv.org/abs/1801.04381)
# """


# WEIGHTS_PATH_X = "https://github.com/bonlime/keras-deeplab-v3-plus/releases/download/1.1/deeplabv3_xception_tf_dim_ordering_tf_kernels.h5"
# WEIGHTS_PATH_MOBILE = "https://github.com/bonlime/keras-deeplab-v3-plus/releases/download/1.1/deeplabv3_mobilenetv2_tf_dim_ordering_tf_kernels.h5"

   # load weights

#     if weights == 'pascal_voc':
#         if backbone == 'xception':
#             weights_path = get_file('deeplabv3_xception_tf_dim_ordering_tf_kernels.h5',
#                                     WEIGHTS_PATH_X,
#                                     cache_subdir='models')
#         else:
#             weights_path = get_file('deeplabv3_mobilenetv2_tf_dim_ordering_tf_kernels.h5',
#                                     WEIGHTS_PATH_MOBILE,
#                                     cache_subdir='models')
            
#         model.load_weights(weights_path, by_name=True)
