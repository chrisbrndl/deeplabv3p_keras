import setuptools
import platform

# requirements
install_requires = open('requirements.txt', 'r').read().split('\n') 

# packages
packages=setuptools.find_packages()

# version read from _version.py in the package
exec(open("deeplabv3p/_version.py").read())
v = __version__

# setup
setuptools.setup(
    name="deeplabv3p",
    version=v,
    author="Christian Brendel",
    author_email="brendel.chris@googlemail.com",
    description="tba",
    long_description="tba",
    long_description_content_type="text/markdown",
    url="tba",
    packages=packages,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: tba :: tba",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=install_requires,
    include_package_data=True
)




